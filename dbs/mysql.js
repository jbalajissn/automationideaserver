
// this helps us to create mysql pool

const config = require('../utils/configs');
const mysql = require('mysql');
var pool;

module.exports = {
    getPool: function(){
        if(pool) return pool;
        pool = mysql.createPool({
            host: config.mysql.host,
            user: config.mysql.username,
            password: config.mysql.password,
            database: config.mysql.database,
            connectionLimit: config.mysql.connectionLimit
        })
        return pool;
    }
}