// we will storing all the configs here 
module.exports = {
    mysql: {
        host: 'localhost',
        username: 'InnovationCouncil',
        password: 'Life@123',
        database: 'innovationcouncil',        
        connectionLimit: 10
    },    
    mysqlDBCodes: {
        connErr: {
            code: 101,
            message: 'MySQL not connected'
        }
    },
    // password encryption keys 
    passEnKey: 'somebigsecreykeywhichshouldnotbeshared',
    daysToBeFinished : 10
}