// this file will do encryption and descryption of passwords 
const config = require('./configs');
const CryptoJS = require("crypto-js");


module.exports = {
    encryptPassword: async (password)=>{
        // encryption of password
        return CryptoJS.AES.encrypt(password, config.passEnKey).toString();
    },
    // descryption of password
    decryptPassword: async (password)=>{
        return CryptoJS.AES.decrypt(password.toString(),config.passEnKey).toString(CryptoJS.enc.Utf8);
    },
    // checking the password whether correct or not 
    checkPassword: async (encryptedPassword,password)=>{
        return CryptoJS.AES.decrypt(encryptedPassword.toString(),config.passEnKey).toString(CryptoJS.enc.Utf8) == password;
    }
}