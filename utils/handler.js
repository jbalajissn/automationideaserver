module.exports = {
    yahResposne: function(res,data,req){
        res.status(200).json({status: true, data: data})
        if(req != undefined && req != null) releaseConnection(req);
    },
    nahReposne: function(res,err,message,code,req){
        if(req != undefined && req != null) releaseConnection(req)
        if(err != null) logError(err)
        res.status(200).json({status: false, message: message, code: code})
    }
}

// this is for logging the error 
function logError(err){
    console.log(err.stack)
}

// release mysql connection 
function releaseConnection(req){
    if(req.mysqlConn != undefined) req.mysqlConn.release();
}