
// this will create a connection for mysql database and attch it to req
const mysqlDB = require('../dbs/mysql');
const handler = require('../utils/handler');
const config  = require('../utils/configs');
module.exports = {
    getConnection: function(req,res,next){
        mysqlDB.getPool().getConnection((err,connection)=>{
           if(err){
               handler.nahReposne(res,err,config.mysqlDBCodes.connErr.message,config.mysqlDBCodes.connErr.code)
           }else{
               req.mysqlConn = connection;
               next();
           }
        })
    }
}