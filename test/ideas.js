// this will test ideas APIs

// set env to test 
process.env.NODE_ENV = 'test';
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();
let app = require('../app');
var assert = require('chai').assert;
var expect = require('chai').expect;
chai.use(chaiHttp);

// getting all the idea 
describe('/POST ideas',()=>{
    it('some test rules here ',(done)=>{
        chai.request(app).post('/ideas/getIdeas').send({}).end((err,res)=>{
            res.should.have.status(200);
            res.should.be.a('object')
            expect(res).to.have.property('status');
            expect(res.body).to.have.property('data');
            done();
        })
    })
})