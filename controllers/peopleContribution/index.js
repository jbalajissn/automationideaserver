const router = require('express').Router();

router.post("/insertpeopleContribution", require("./insertPeopleContribution").insertpeopleContribution);
router.post("/insertPeopleTrainingEnrollment", require("./insertPeopleContribution").TrainingEnrollment);
router.get("/getAllTrainings", require("./ShowTraining").showAllTraining);
router.get("/getUpComingTrainings", require("./ShowTraining").showUpComingTraining);
router.post("/showTrainingBetweenDates", require("./ShowTraining").ShowTrainingBetweenDates);
router.post("/showTrainingByMode", require("./ShowTraining").ShowTrainingByMode);
router.post("/showTrainingBySkill", require("./ShowTraining").ShowTrainingBySkill);
router.post("/showTrainingById", require("./ShowTraining").showTrainingDetail);
router.post("/updateTrainingDetails",require("./ShowTraining").updateTrainingDetail);
router.post("/getregisteredTraining",require("./ShowTraining").getTrainingDetailsById);
router.get("/getTrainingCount",require("./ShowTraining").getTrainingEnrollCount);
router.get("/userEnrolledcountbyType",require("./ShowTraining").getEnrolledcountbyType);
router.get("/getEnrolledUserCntbyMonth",require("./ShowTraining").getEnrolledUserCountbyMonth);
module.exports = router;