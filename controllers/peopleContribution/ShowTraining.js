const services = require('../../services/peopleContribution/peopleService');
const handler = require('../../utils/handler');
module.exports = {
    showAllTraining : async function(req,res){
        // get all the Automation Towers 
        try{
            const data = await services.getAllTraining(req.mysqlConn);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in getting orders",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    showUpComingTraining:async function(req,res){
        try{
            const data = await services.getUpcomingTraining(req.mysqlConn);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in getting orders",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    ShowTrainingBetweenDates:async function(req,res){
        try{
            const data = await services.getTrainingBetweenDates(req.mysqlConn,req.body.TrainingStartDate1,req.body.TrainingStartDate2);
            handler.yahResposne(res,data);
       }catch(error){
            handler.nahReposne(res,error,"error in getting orders",202);
        }
    },
    ShowTrainingByMode:async function(req,res){
        try{
            const data = await services.getTraingbyMode(req.mysqlConn,req.body.mode);
            handler.yahResposne(res,data);
       }catch(error){
            handler.nahReposne(res,error,"error in getting orders",202);
        }
    },
    ShowTrainingBySkill:async function(req,res){
        try{
            const data = await services.getTraingbySkill(req.mysqlConn,req.body.TrainingSkillType);
            handler.yahResposne(res,data);
       }catch(error){
            handler.nahReposne(res,error,"error in getting orders",202);
        }
    },
        showTrainingDetail:async function(req,res){
            try{
                console.log(req.body)
                const data = await services.getTrainingdetail(req.mysqlConn,req.body.id);
                handler.yahResposne(res,data);
           }catch(error){
                handler.nahReposne(res,error,"error in getting orders",202);
            }
        },
        updateTrainingDetail:async function(req,res){
            try{
                //console.log(req.body);
              // var TrainingStartDate=new Date(req.body.data.TrainingStartDate).UTC();
              // console.log((req.body.data.TrainingStartDate).toGMTString());
               //console.log(TrainingStartDate);
                const data=await services.updateTrainingDetail(req.mysqlConn,req.body.data.TrainingDescription.value,req.body.data.TrainingMode,req.body.data.TrainingType,req.body.data.TrainingSkillType,req.body.data.TrainingStartDate,req.body.data.TrainingEndDate,req.body.data.TargetAudience,req.body.data.TrainingLocation,req.body.data.TrainingPrequisite,req.body.data.TrainingComments,req.body.data.id);
                
                handler.yahResposne(res,data);
            }catch(error){
                handler.nahReposne(res,error,"error in getting orders",202);

            }
           
        },
        getTrainingDetailsById:async function(req,res){
            try{
                console.log(req.body);
                const data=await services.getTrainingByEmpID(req.mysqlConn,req.body.id);
                handler.yahResposne(res,data);
            }catch(error){
                handler.nahReposne(res,error,"error in getting orders",202);
            }
        },
        getTrainingEnrollCount:async function(req,res){
            try{
               // console.log(req.body);
                const data=await services.getEnrolledUserCount(req.mysqlConn);
                handler.yahResposne(res,data);
            }catch(error){
                handler.nahReposne(res,error,"error in getting details",202);
            }

        },
        getEnrolledcountbyType:async function(req,res){
            try{
                const data=await services.getEnrolledUserCountBySkillType(req.mysqlConn);
                handler.yahResposne(res,data);
            }catch(error){
                handler.nahReposne(res,error,"error in getting details",202);
            }
        
        },
        getEnrolledUserCountbyMonth:async function(req,res){
            try{
                const data=await services.getEnrolledUserCountbyMonth(req.mysqlConn);
                handler.yahResposne(res,data);
            }catch(error){
                handler.nahReposne(res,error,"error in getting details",202);
            }
        

        }
    
   
}