
const services = require('../../services/peopleContribution/insertPeopleContributionService');
const handler = require('../../utils/handler');
module.exports = {
    insertpeopleContribution : async function(req,res){
        // get all the Automation Towers 
        try{
            const data = await services.insertpeopleContribution(req.mysqlConn,req.body.data);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in inserting data",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    TrainingEnrollment:async function(req,res){
        try{
            const data=await services.TrainingEnrollment(req.mysqlConn,req.body.data);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in inserting data",202);
        }finally{
            req.mysqlConn.release();
        }
    }
}