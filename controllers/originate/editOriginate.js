const services = require('../../services/originate/saveoriginate');
const handler = require('../../utils/handler');
module.exports = {
    editOriginateDetails : async function(req,res){
        try {
            const data= await services.updateOriginate(req.mysqlConn,req.body.data.OriginateDesc,
                req.body.data.OriginateCategoryType,req.body.data.OriginateACNStakeholder,req.body.data.OriginateCLNStakeholder,
                req.body.data.OriginateETCDate,req.body.data.OriginateStatusType,req.body.data.ProposedCCI,req.body.data.ApprovedCCI,
                req.body.data.OriginateProbability,req.body.data.OriginateEstimate,req.body.data.OriginateRevenue,
                req.body.data.OriginateApprover,req.body.data.OriginateComments,req.body.data.OriginateID);
            const insertComments = {
                OriginateID: req.body.data.OriginateID,
                OriginateComments: req.body.data.OriginateComments,
                OriginateCommentsBy: req.body.data.OriginateCreatedBy
            };
            const insertData = await services.insertOriginateComments(req.mysqlConn,insertComments);
            handler.yahResposne(res,insertData);
        }catch(error){
            handler.nahReposne(res,error,"error in updating details",202);
        }finally{
            
            req.mysqlConn.release();
        }
    }
}
