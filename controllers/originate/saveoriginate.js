const services = require('../../services/originate/saveoriginate');
const handler = require('../../utils/handler');
module.exports = {
    saveoriginatedetails : async function(req,res){
        // get all the ideas 
        try{
            const result = await services.insertOriginate(req.mysqlConn,req.body.data);
            if(result > 0)
            {
                const insertComments = {
                    OriginateID: result,
                    OriginateComments: req.body.data.OriginateComments,
                    OriginateCommentsBy: req.body.data.OriginateCreatedBy
                };
                const insertData = await services.insertOriginateComments(req.mysqlConn,insertComments);
            }
            handler.yahResposne(res,result);
        }catch(error){
            handler.nahReposne(res,error,"error in saving originate details",202);
        }finally{
            req.mysqlConn.release();
        }
    }
}
