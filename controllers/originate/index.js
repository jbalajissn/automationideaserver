const router = require("express").Router();

router.get("/getOriginateEntity", require("./getOriginate").getOriginateEntity);
router.get("/getOriginateDetails", require("./getOriginate").getOriginateDetails);
router.post("/saveOriginateDetails", require("./saveoriginate").saveoriginatedetails);
router.post("/editOriginateDetails", require("./editOriginate").editOriginateDetails);
router.post("/getSearchOriginate", require("./getOriginate").getSearchOriginate);
router.post("/getCommentsOriginate",require("./getOriginate").getCommentsOriginate);

module.exports = router;