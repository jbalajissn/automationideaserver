const services = require('../../services/originate/originateDetailService');
const handler = require('../../utils/handler');
module.exports = {
    getOriginateEntity : async function(req,res){
        // get all the ideas 
        try{
            const data = await services.getOriginateEntity(req.mysqlConn);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in getting orders",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    getOriginateDetails : async function(req,res){
        // get all the ideas 
        try{
            const data = await services.getOriginateDetails(req.mysqlConn);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in getting orders",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    getSearchOriginate : async function(req,res){
        // get all the Automation Towers 
        try{
            console.log(req.body);
            const data = await services.getSearchOriginate(req.mysqlConn,req.body.strSearch);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in getting orders",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    getCommentsOriginate : async function(req,res){
        // get all the Automation Towers 
        try{
            console.log(req.body);
            const data = await services.getCommentsOriginate(req.mysqlConn,req.body.strSearch);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in getting comments Originate",202);
        }finally{
            req.mysqlConn.release();
        }
    }
}