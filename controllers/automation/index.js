const router = require('express').Router();

// this is to get Automation Towers dropdown
router.get("/getDetails",require("./getAutomationTower").getAutomationDetails);
router.get("/getStatus",require("./getAutomationTower").getAutomationStatus);
router.get("/getEntity",require("./getAutomationTower").getAutomationEntity);
router.get("/getTowers", require("./getAutomationTower").getAutomationTower);
router.get("/getApplications", require("./getAutomationTower").getAppliationName);
router.post("/insertAutomation", require("./insertAutomation").insertAutomation);
router.get("/getAllAutomation", require("./showAutomationList").showAllAutomations);
router.post("/getSearchAutomation", require("./showAutomationList").getSearchAutomation);
/*banu--router.post("/getEditAutomation", require("./showAutomationList").getEditAutomation);*/
router.post("/getEditAutomation", require("./showAutomationList").getEditAutomation);
router.post("/updateAutomationDetails",require("./showAutomationList").updateAutomationDetails);
router.post("/getCommentsHistory",require("./showAutomationList").showAutomationsHistory);
module.exports = router;