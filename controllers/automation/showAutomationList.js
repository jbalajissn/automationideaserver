
const services = require('../../services/automation/showAutomationListService');
const handler = require('../../utils/handler');

module.exports = {
    showAllAutomations : async function(req,res){
        // get all the Automation Towers 
        try{
            const data = await services.getAllAutomation(req.mysqlConn);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in getting orders",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    getSearchAutomation : async function(req,res){
        // get all the Automation Towers 
        try{
            console.log(req.body);
            const data = await services.getSearchAutomation(req.mysqlConn,req.body.strSearch,req.body.strfield);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in getting orders",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    getEditAutomation : async function(req,res){
        try{
            const data = await services.getEditAutomation(req.mysqlConn,req.body.id);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in getting orders",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    updateAutomationDetails: async function(req,res){
        try {
            console.log("Request Body");
            console.log(req.body);
            const data=await services.updateAutomationDetails(req.mysqlConn,req.body.data.status,req.body.data.comment,req.body.data.lead,req.body.data.resource,
                req.body.data.automationWorkType,req.body.data.automationBenefitArea,
                req.body.data.automationInitiativeCat,req.body.data.automationTool,req.body.data.automationModifiedUser,
                req.body.data.creationdate,req.body.data.completiondate,req.body.data.automationEffort,
                req.body.data.automationFTE,req.body.data.automationcostperday,req.body.data.automationnetcostsaving,
                req.body.data.automationticket,req.body.data.automationImplementation,req.body.data.id);
            const history_data = {
                    autodetailsID : req.body.data.id,
                    autodetailsComments : req.body.data.comment,
                    autodetailsUser : req.body.data.automationModifiedUser
                }
                const insertData = await services.insertAutomationHistory(req.mysqlConn,history_data);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in updating details",202)
        }finally{
            
            req.mysqlConn.release();
        }
    },
    showAutomationsHistory : async function(req,res){
        // get all the Automation Towers 
        try{
            const data = await services.getAutomationHistory(req.mysqlConn,req.body.id);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in getting orders",202);
        }finally{
            req.mysqlConn.release();
        }
    }
}