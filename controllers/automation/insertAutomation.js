
const services = require('../../services/automation/insertAutomationService');
const handler = require('../../utils/handler');
module.exports = {
    insertAutomation : async function(req,res){
        // get all the Automation Towers 
        try{
            const data = await services.insertAutomation(req.mysqlConn,req.body.data);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in inserting data",202);
        }finally{
            req.mysqlConn.release();
        }
    }
}