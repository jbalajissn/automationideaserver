
const services = require('../../services/automation/automationDashboardServices');
const handler = require('../../utils/handler');
module.exports = {
    getAutomationTower : async function(req,res){
        // get all the Automation Towers 
        try{
            const data = await services.getAutomationTower(req.mysqlConn);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in getting orders",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    
    getAutomationDetails : async function(req,res){
        // get all the AutomationList 
        try{
            const data = await services.getAutomationDetails(req.mysqlConn);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in getting orders",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    getAutomationEntity : async function(req,res){
        // get all the AutomationList 
        try{
            const data = await services.getAutomationEntity(req.mysqlConn);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in getting orders",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    getAutomationStatus : async function(req,res){
        // get all the Automation Status 
        try{
            const data = await services.getAutomationStatus(req.mysqlConn);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in getting Status",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    getAppliationName : async function(req,res){
        // get all the AutomationList 
        try{
            const data = await services.getAutomationAppName(req.mysqlConn);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in getting orders",202);
        }finally{
            req.mysqlConn.release();
        }
    }
    
}