const router = require('express').Router();


// this is for login for both admin and normal users 
router.post('/adminLogin',require('./login').adminLogin);
router.post('/userLogin',require('./login').userLogin);

// this is for signup for both normal and admin users 
router.post('/adminSignUp',require('./signup').adminSignUp);
router.post('/userSignUP',require('./signup').userSignUp)

module.exports = router;

