const services = require('../../services/auth/authServices');
const handler = require('../../utils/handler');
const passWordService = require('../../utils/password');

module.exports = {
    // this is for admin login
    adminLogin: async (req,res) =>{
        try{
            console.log(req.mysqlConn);
            const result = await services.adminLogin(req.mysqlConn,req.body.email);
            if(result.length == 0){
                handler.nahReposne(res,null,"check credentials",201,req)
            }else{
                // check password by encrypting it 
                const isPasswordCorrect = await passWordService.checkPassword(result[0].password,req.body.password);
                (isPasswordCorrect ? (handler.yahResposne(res,result,req)) : (handler.nahReposne(res,null,"check credentials",201,req)))
            }
        }catch(error){
            handler.nahReposne(res,error,"login failed",202,req)
        }
    },
    // this is for non-admin user logins 
    userLogin: async (req,res)=>{
        try{
            // remember 
            const result = await services.userLogin(req.mysqlConn,req.body.email);
            if(result.length == 0){
                handler.nahReposne(res,null,"check credentials",201,req)
            }else{
                // check password by encrypting it 
                const isPasswordCorrect = await passWordService.checkPassword(result[0].password,req.body.password);
                (isPasswordCorrect ? (handler.yahResposne(res,result,req)) : (handler.nahReposne(res,null,"check credentials",201,req)))
            }
        }catch(error){
            handler.nahReposne(res,error,"login failed",202,req)
        }
    }
}