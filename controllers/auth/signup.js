const handler = require('../../utils/handler');
const services = require('../../services/auth/signupServices')
const passEn = require('../../utils/password');

module.exports = {
    // signing up admin user 
    adminSignUp: async (req,res)=>{
        try{
            // first check same user with email exists or not 
            const emailData = await services.checkEmail(req.mysqlConn,req.body.email);
            if(emailData.length == 0){
                const encrypTedPassword = await passEn.encryptPassword(req.body.password);
                // user_group_id_fk 1 for admin 
                const data = {
                    user_group_id_fk : 1,
                    email: req.body.email,
                    password: encrypTedPassword,
                    username: req.body.username
                }
                // now save the user data in database 
                const userData = await services.createAdmin(req.mysqlConn,data);
                handler.yahResposne(res,data,userData)
            }else{
                handler.nahReposne(res,null,"user already exists",101);
            }
        }catch(error){
            handler.nahReposne(res,error,"signup error",101);
        }finally{
            req.mysqlConn.release();
        }
    },
    userSignUp: async (req,res)=>{
        try{
            // first check same user with email exists or not 
            const emailData = await services.checkEmail(req.mysqlConn,req.body.email);
            if(emailData.length == 0){
                const encrypTedPassword = await passEn.encryptPassword(req.body.password);
                console.log(encrypTedPassword);
                // user_group_id_fk 2 for normal user 
                const data = {
                    user_group_id_fk : 2,
                    email: req.body.email,
                    password: encrypTedPassword,
                    username: req.body.username
                }
                // now save the user data in database 
                const userData = await services.createAdmin(req.mysqlConn,data);
                handler.yahResposne(res,data,userData)
            }else{
                handler.nahReposne(res,null,"user already exists",101);
            }
        }catch(error){
            handler.nahReposne(res,error,"signup error",101);
        }finally{
            req.mysqlConn.release();
        }
    }
}