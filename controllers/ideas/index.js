const router = require("express").Router();

router.post("/insert", require("./insert").insertIdeas);
router.post("/getIdeas", require("./getIdeas").getIdeas);
router.post("/clientIdeas", require("./insert").insertClinetData);
router.post('/updateLike',require('./ideasUtils').updateLike)
router.post('/getLikesCount',require('./ideasUtils').getLikesCount)
router.post('/updateApproval',require('./ideasUtils').updateApproveStatus);
router.post('/getPhasesData',require('./ideasUtils').getAllAphasesData);
router.post('/getApprovalHistory',require('./ideasUtils').getApprovalHistory);
router.post('/getFeasibilityHistory',require('./ideasUtils').getFeasibilityHistory);
router.post('/updateFeasibilityStatus',require('./ideasUtils').updateFeasibilityStatus);
router.post('/getOverdueStatus',require('./ideasUtils').getOverdueStatus);
router.post('/insertTeamData',require('./ideasUtils').insertTeamData);
router.post('/getTeamData',require('./ideasUtils').getTeamData);
router.post('/insertBuildData',require('./ideasUtils').insertBuildData);
router.post('/getBuildData',require('./ideasUtils').getBuildData);
router.post('/getProductionData',require('./ideasUtils').getProductionData);
router.post('/insertProductionData',require('./ideasUtils').insertProductionData);
router.post('/searchIdeaDetails',require('./getIdeas').searchIdeas);
router.post('/getBenefitsdata',require('./ideasUtils').getBusinessBenefitsData);
router.post('/saveBenefitsdata',require('./ideasUtils').insertBusinessBenefitsData);
router.post('/getQuarter',require('./ideasUtils').getQuarterData);
router.post('/gethours',require('./ideasUtils').updateFeasibilityStatus);
router.get("/getTechIdeacount", require("./getIdeas").getIdeaCountbyTech);
router.get("/getTowerIdeacount", require("./getIdeas").getIdeaCountbyTower);
router.get("/getStatusIdeacount", require("./getIdeas").getIdeaCountbyStatus);
router.get("/getQuarterIdeacount", require("./getIdeas").getIdeaCountbyQuarter);

module.exports = router;
