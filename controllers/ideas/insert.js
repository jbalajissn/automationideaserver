const handler = require('../../utils/handler');
const insertServices = require('../../services/ideas/insertService');

module.exports = {
    insertIdeas :async function(req,res){
        try{
            const ideasData = await insertServices.insertIdeas(req.mysqlConn,req.body.data);
            handler.yahResposne(res,ideasData);
        }catch(error){
            handler.nahReposne(res,error,"insert failed",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    insertClinetData: async (req,res)=>{
        try{
            const ideasData = await insertServices.insertClientData(req.mysqlConn,req.body.data);
            req.mysqlConn.release();
            handler.yahResposne(res,ideasData);
        }catch(error){
            handler.nahReposne(res,error,"insert failed",202);
        }finally{
            req.mysqlConn.release();
        }
    }
}