// this file helps us in getting, sorting ideas...
const services = require('../../services/ideas/ideasServices');
const handler = require('../../utils/handler');
module.exports = {
    getIdeas : async function(req,res){
        // get all the ideas 
        try{
            const data = await services.getIdeas(req.mysqlConn);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in getting orders",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    searchIdeas : async function(req,res){
        try{
            const data = await services.getSearchIdeas(req.mysqlConn,req.body.strSearch); 
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in getting orders",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    getIdeaCountbyTech:async function(req,res){
        try{
            const data=await services.getIdeaCountbyTech(req.mysqlConn);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in fetching details",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    getIdeaCountbyTower:async function(req,res){
        try{
            const data=await services.getIdeaCountbyTower(req.mysqlConn);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in fetching details",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    getIdeaCountbyStatus:async function(req,res){
        try{
            const data=await services.getIdeaCountbyStatus(req.mysqlConn);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in fetching details",202);
        }finally{
            req.mysqlConn.release();
        }
    },
    getIdeaCountbyQuarter:async function(req,res){
        try{
            const data=await services.getIdeaCountbyQuarter(req.mysqlConn);
            handler.yahResposne(res,data);
        }catch(error){
            handler.nahReposne(res,error,"error in fetching details",202);
        }finally{
            req.mysqlConn.release();
        }
    }
}