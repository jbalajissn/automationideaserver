/*
    this file for idea utils such as sorting them, updating likes etc..
    @saiumesh 27th DEC
*/
const handler = require('../../utils/handler');
const services = require('../../services/ideas/ideasServices');

module.exports = {
    updateLike: async (req,res)=>{
        try{
            const likeData  = await services.updateLike(req.mysqlConn,req.body.count,req.body.id);
            handler.yahResposne(res,likeData,req)
        }catch(error){
            handler.nahReposne(res,error,"update like failed",101,req);
        }
    },
    getLikesCount : async (req,res)=>{
        try{
            const likeData  = await services.getLikeCount(req.mysqlConn,req.body.id);
            handler.yahResposne(res,likeData,req)
        }catch(error){
            handler.nahReposne(res,error,"update like failed",101,req);
        }
    },
    // updating the approval status 
    updateApproveStatus: async (req,res)=>{
        try{
            // first update status in ideas table and then insert the data 
            const approveData = await services.updateApproveSttaus(req.mysqlConn,req.body.id,req.body.status);
            const data = {
                idea_id_fk: req.body.id,
                comments: req.body.comments,
                status: req.body.status,
                created_by: req.body.created_by
            }
            const insertData = await services.insertApprovalData(req.mysqlConn,data);
            handler.yahResposne(res,approveData,req)
        }catch(error){
            handler.nahReposne(res,error,"update like failed",101,req);
        }
    },
    // getting all the phases data 
    getAllAphasesData : async (req,res) =>{
        // a biiiiiiig promise 
        try{
            const biiiigData = await Promise.all([
                services.getApprovedHistory(req.mysqlConn,req.body.id),
                services.getFeasibility(req.mysqlConn,req.body.id)
            ])
            handler.yahResposne(res,biiiigData,req)
        }catch(error){
            handler.nahReposne(res,error,"update like failed",101,req);
        }
    },
	
	getQuarterData : async (req,res) =>{
    try{
        const data =  await services.getQuarterData(req.mysqlConn,req.body.id);
        handler.yahResposne(res,data,req)
    }catch(error){
        handler.nahReposne(res,error,"something went wrong",101,req);
        }
    },  
    // this is for getting all the approval history
    getApprovalHistory : async (req,res)=>{
        try{
            const approvalHistory = await services.getApprovedHistory(req.mysqlConn,req.body.id);
            handler.yahResposne(res,approvalHistory,req);
        }catch(error){
            handler.nahReposne(res,error,"getting data failed",101,req);
        }
    },
   /* updateFeasibilityStatus : async (req,res)=>{
        try{
            // first update status then insert the data in table 
            const updateData = await services.updateFesibilityStatus(req.mysqlConn,req.body.id,req.body.status);
            // now insert the data 
            const data = {
                idea_id_fk: req.body.id,
                comments: req.body.comments,
                status: req.body.status,
                created_by: req.body.created_by
            }
            const insertData = await services.insertFeasibilityData(req.mysqlConn,data);
            handler.yahResposne(res,insertData,req)
        }catch(error){
            handler.nahReposne(res,error,"something went wrong",101,req);
        }
    },*/
	updateFeasibilityStatus : async (req,res)=>{
        try{
            // first update status then insert the data in table 
            const updateData = await services.updateFesibilityStatus(req.mysqlConn,req.body.id,req.body.status,req.body.quarterhours,req.body.quarter);
            // now insert the data 
            const data = {
                idea_id_fk: req.body.id,
                comments: req.body.comments,
                quarterhours  : req.body.quarterhours,
                quarter : req.body.quarter,
                status: req.body.status,
                created_by: req.body.created_by
            }
            const insertData = await services.insertFeasibilityData(req.mysqlConn,data);
            handler.yahResposne(res,insertData,req)
        }catch(error){
            handler.nahReposne(res,error,"something went wrong",101,req);
        }
    },
    getFeasibilityHistory : async (req,res) =>{
        try{
            const data =  await services.getFeasibility(req.mysqlConn,req.body.id);
            handler.yahResposne(res,data,req)
        }catch(error){
            handler.nahReposne(res,error,"something went wrong",101,req);
        }
    },
    getOverdueStatus : async (req,res) => {
        try{
            const allData = await Promise.all(
                            [services.getApprovedHistory(req.mysqlConn,req.body.id),
                             services.getFeasibility(req.mysqlConn,req.body.id)]);             
            const result = {
                approvalData : allData[0][allData[0].length - 1],
                feasibilityData : allData[1][allData[1].length - 1]
            }                 
            handler.yahResposne(res,result,req)                 
        }catch(error){
            handler.nahReposne(res,error,"something went wrong",101,req);
        }
    },
    insertTeamData : async (req,res)=>{
        try{
             /* this is for inserting team data */
             // before inserting the data update team status in ideas table
            const updateTeam = await services.updateTeamStatus(req.mysqlConn,1,req.body.id);
            const data = {
                idea_id_fk : req.body.id,
                team_memebers : req.body.team_memebers,
                created_by : req.body.created_by
            }
            const teamData = await services.insertTeamData(req.mysqlConn,data);
            handler.yahResposne(res,teamData,req) 
        }catch(error){
            handler.nahReposne(res,error,"something went wrong",101,req);
        }
    },
    // getting the recent team data
    getTeamData : async (req,res) =>{
        const teamData = await services
                .getTeamData(req.mysqlConn,req.body.id)
                .catch((error)=>{handler.nahReposne(res,error,"something went wrong",101,req);})
        if(teamData) {
            if(teamData.length > 0) handler.yahResposne(res,teamData,req)
            else handler.nahReposne(res,null,"No Team data found",101,req)
        }
    },
    /* insert the build phases data */
    insertBuildData : async (req,res) =>{
        try{
             
            // update the build status for the idea 
            const updateStatus = await services.updateBuildStatus(req.mysqlConn,req.body.id,req.body.status);
            //  create data object and insert the data 
            const data = {
                idea_id_fk : req.body.id,
                title : req.body.title,
                start_date : req.body.start_date,
                end_date : req.body.end_date,
                created_by : req.body.created_by
            }
            const buildData = await services.insertBuildData(req.mysqlConn,data);
            handler.yahResposne(res,buildData,req)
        }catch(error){
            handler.nahReposne(res,error,"something went wrong",101,req)
        }
    },
    /* getting the build data */
    getBuildData : async (req,res) =>{
        try{
            const buildData = await services.getBuildData(req.mysqlConn,req.body.id);
            handler.yahResposne(res,buildData,req);
        }catch(error){
            handler.nahReposne(res,error,"something went wrong",101,req)
        }
    },
    /* inserting production data */
    insertProductionData : async(req,res) => {
        try{
            // first update the status then insert the data 
            const updateStatus = await services.updateProductionStatus(req.mysqlConn,req.body.id,req.body.status);
            const data = {
                idea_id_fk : req.body.id,
                title : req.body.title,
                start_date : req.body.start_date,
                end_date : req.body.end_date,
                created_by : req.body.created_by
            }
            const insertData = await services.insertProductionData(req.mysqlConn,data);
            handler.yahResposne(res,insertData,req);
        }catch(error){
            handler.nahReposne(res,error,"something went wrong",101,req)
        }
    },
    /* get production data */
    getProductionData : async (req,res) => {
        try{
            const productionData = await services.getProductionData(req.mysqlConn,req.body.id);
            handler.yahResposne(res,productionData,req);
        }catch(error){
            handler.nahReposne(res,error,"something went wrong",101,req)
        }
    },

    getBusinessBenefitsData : async (req,res) => {
        try{
            const businessBenefitsData = await services.getBusBenefitData(req.mysqlConn,req.body.id);
            handler.yahResposne(res,businessBenefitsData,req);
        }catch(error){
            handler.nahReposne(res,error,"something went wrong",101,req)
        }
    },
    /* inserting businessbenefits data */
    insertBusinessBenefitsData : async(req,res) => {
        try{
            // first update the status then insert the data 
            const updateStatus = await services.updateBusinessBenefitStatus(req.mysqlConn,req.body.id,req.body.status);
            const data = {
                idea_id_fk : req.body.id,
                businessBenefits  : req.body.businessBenefits ,
                businessAppreciation  : req.body.businessAppreciation ,
                businessbenefitsClassification : req.body.businessbenefitsClassification ,
                benefitsHours : req.body.benefitsHours ,
                created_by : req.body.created_by 
            }
            const insertData = await services.saveBusinessBenefitData(req.mysqlConn,data);
            handler.yahResposne(res,insertData,req);
        }catch(error){
            handler.nahReposne(res,error,"something went wrong",101,req)
        }
    }
}