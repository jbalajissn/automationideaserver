//import { connect } from 'tls';

const appServices = require('../appService');
const mysql = require('mysql');

module.exports = {
    getIdeas: async function(connection){
        const query = "select * from ideas ORDER BY created_at DESC";
        return await appServices.runQuery(connection,query);
    },
    updateLike : async (connection,count,id)=>{
        const query = mysql.format("update ideas set likes = ? where id = ?",[count,id]);
        return await appServices.runQuery(connection,query);
    },
    getLikeCount : async (connection,id)=>{
        const query = mysql.format("select * from ideas where id = ?",[id]);
        return await appServices.runQuery(connection,query);
    },
    updateApproveSttaus: async(connection,id,status)=>{
        const query = mysql.format("update ideas set approval_status = ? where id = ?",[status,id]);
        return await appServices.runQuery(connection,query);
    },
    insertApprovalData : async (connection,data)=>{
        const query = mysql.format("insert into approval set ?",[data]);
        return await appServices.runQuery(connection,query);
    },
    getFeasibility: async(connection,id)=>{

        const query = mysql.format("select feasibility.id as id,idea_id_fk,comments,(case  when status= 0 then 'Dis Approved' else 'Approved' end) as 'status' ,\
                            DATE_FORMAT(created_at,'%Y-%m-%d') as created_at, \
                            users.username  from feasibility inner join users on \
                            users.id = feasibility.created_by WHERE idea_id_fk = ?",[id]);
        return await appServices.runQuery(connection,query);
    },
	getQuarterData: async(connection)=>{
        const query = mysql.format("select * from automationentity where automationEntityType = 'Quarter'");
        return await appServices.runQuery(connection,query);
    },
    getApprovedHistory: async(connection,id)=>{
        const query = mysql.format("select approval.id as id,idea_id_fk,comments,(case  when status= 0 then 'Dis Approved' else 'Approved' end) as 'status' ,\
        date(created_at) as created_at, users.username  from approval inner join users on users.id = approval.created_by WHERE idea_id_fk= ?",[id]);
        return await appServices.runQuery(connection,query);
    },
   /* updateFesibilityStatus: async (connection,id,status)=>{
        const query = mysql.format("update ideas set feasibility_status = ? where id = ?",[status,id]);
        return await appServices.runQuery(connection,query);
    },*/
	updateFesibilityStatus: async (connection,id,status,quarterhours,quarter)=>{
        const query = mysql.format("update ideas set feasibility_status = ?,quarterhours = ?,quarter =? where id = ?",[status,quarterhours,quarter,id]);
        return await appServices.runQuery(connection,query);
    },
    insertFeasibilityData : async (connection,data)=>{
        const query = mysql.format("INSERT INTO feasibility set ? ",[data]);
        return await appServices.runQuery(connection,query);
    },
    insertTeamData : async (connection,data)=>{
        const query = mysql.format("insert into team set ?",[data]);
        return await appServices.runQuery(connection,query);
    },
    updateTeamStatus : async (connection,status,id) =>{
        const query = mysql.format("update ideas set team_status = ? where id = ?",[status,id]);
        return await appServices.runQuery(connection,query);
    },
    getTeamData : async (connection,idea_id_fk) => {
        const query = mysql.format("select * from team where idea_id_fk = ? order by created_at DESC limit 1",[idea_id_fk]);
        return await appServices.runQuery(connection,query);
    },
    insertBuildData : async (connection,data) => {
        const query = mysql.format("insert into build set ?",[data]);
        return await appServices.runQuery(connection,query);
    },
    updateBuildStatus : async (connection,id,status) =>{
        const query = mysql.format("update ideas set build_status = ? where id = ?",[status,id]);
        return await appServices.runQuery(connection,query);
    },
    getBuildData : async (connection,id) =>{
        const query = mysql.format("select * from build where idea_id_fk = ? order by created_at DESC",[id]);
        return await appServices.runQuery(connection,query);
    },
    updateProductionStatus : async (connection,id,status) =>{
        const query = mysql.format("update ideas set production_status = ? where id = ?",[status,id]);
        return await appServices.runQuery(connection,query);
    },
    insertProductionData : async (connection,data) =>{
        const query = mysql.format("insert into production set  ?",[data]);
        return await appServices.runQuery(connection,query);
    },
    getProductionData : async (connection, id) =>{
        const query = mysql.format("select * from production where idea_id_fk = ?",[id]);
        return await appServices.runQuery(connection,query);
    },
    getSearchIdeas : async (connection, title) =>{
        const query = mysql.format("select * from ideas where title like ? ORDER BY created_at DESC",[title]);
        return await appServices.runQuery(connection,query);
    },
    getBusBenefitData : async (connection, id) =>{
        const query = mysql.format("select * from business_impacts where idea_id_fk = ? ORDER BY created_dt DESC limit 1",[id]);
        return await appServices.runQuery(connection,query);
    },
    saveBusinessBenefitData : async (connection,data) =>{
        const query = mysql.format("insert into business_impacts set  ?",[data]);
        return await appServices.runQuery(connection,query);
    },
    updateBusinessBenefitStatus : async (connection,id,status) =>{
        const query = mysql.format("update ideas set business_impact_status = ? where id = ?",[status,id]);
        return await appServices.runQuery(connection,query);
    },
    getIdeaCountbyTech:async(connection)=>{
        const query=mysql.format("SELECT techs as name,count(1) as value FROM `ideas` GROUP by techs");
        return await appServices.runQuery(connection,query);
    },
    getIdeaCountbyTower:async(connection)=>{
        const query=mysql.format("SELECT tower as name,COUNT(1) as value FROM `ideas` GROUP by tower");
        return await appServices.runQuery(connection,query);
    },
    getIdeaCountbyStatus:async(connection)=>{
        const query=mysql.format("select St as name,count(1) as value from\
        (SELECT id, \
            (CASE When production_status=1 Then 'Production Completed'\
             When build_status =1 Then 'Build Completed'\
              When team_status=1 Then 'TeamCreation Completed'\
               When feasibility_status=1 then 'Feasibility Completed'\
                when approval_status=1 then 'Approval Completed'\
                 when innovate_status=1 then 'Feasibilty Pending' else 'N/A' End )as st from ideas )as STATUS GROUP by St ");
        return await appServices.runQuery(connection,query);
    },
    getIdeaCountbyQuarter:async(connection)=>{
        const query=mysql.format("SELECT automationentity.automationEntityValue as name,sum(quarterhours) as value \
        FROM `ideas` \
        inner join automationentity on automationentity.automationEntityID = ideas.quarter \
        WHERE 1 group by quarter ");
        return await appServices.runQuery(connection,query);
    }
}