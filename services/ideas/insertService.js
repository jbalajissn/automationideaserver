const appServices = require('../appService');
const mysql = require('mysql');

module.exports = {
    // inserting ideas into table 
    insertIdeas: async function(connection,data){
        const query = mysql.format("insert into ideas set ? ",[data]);
        return await appServices.runQuery(connection,query);
    },
    insertClientData: async (connection,data)=>{
        const query = mysql.format("insert into clientideas set ? ",[data]);
        return await appServices.runQuery(connection,query);
    }
}