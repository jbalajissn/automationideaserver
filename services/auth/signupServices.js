const appServices = require('../appService');
const mysql = require('mysql');

module.exports = {
    checkEmail : async (connection,email)=>{
        const query = mysql.format("select email from users where email = ?",[email]);
        return await appServices.runQuery(connection,query);
    },
    createAdmin: async (connection,data)=>{
        const query = mysql.format("insert into users set  ?",[data]);
        console.log(query);
        return await appServices.runQuery(connection,query);
    }
}