const appServices = require('../appService');
const mysql = require('mysql')

module.exports = {
    // this is for normal user login here type 1 is admin 
    userLogin : async (connection,email,password)=>{
        const query = mysql.format("select * from users where email = ?  and user_group_id_fk = ?",[email,2]);
        return await appServices.runQuery(connection,query);
    },
    // this is for admin login, here type is normal user 
    adminLogin: async (connection,email,password) =>{
        const query = mysql.format("select * from users where email = ? and user_group_id_fk = ?",[email,1]);
        return await appServices.runQuery(connection,query);
    }
}