const appServices = require('../appService');
const mysql = require('mysql');

module.exports = {
    getAllAutomation: async function(connection){
        const query = "SELECT automationID,automationTower,automationApplication,automationTitle,automationDescription,\
        DATE_FORMAT(automationActualCompletionDate,'%Y-%m-%d') AS automationActualCompletionDate ,autoS.automationEntityValue AS STATUS  FROM \
        automationdetails auto  JOIN automationentity autoS ON autoS.automationEntityType = 'automationStatus' \
        AND autoS.automationEntityID = auto.automationStatus";
        return await appServices.runQuery(connection,query);
    },

    getSearchAutomation: async function(connection,title,field){
        console.log(title,field);
        const query = mysql.format("SELECT automationID,automationTower,automationApplication,automationTitle,\
        automationDescription,STATUS,\
        DATE_FORMAT(automationActualCompletionDate,'%Y-%m-%d') AS automationActualCompletionDate,\
        automationCreationDate FROM\
        (Select automationID,automationTower,automationApplication,automationTitle,automationDescription,\
             autoS.automationEntityValue AS STATUS,automationCreationDate,automationActualCompletionDate  FROM\
        automationdetails auto  JOIN automationentity autoS ON \
         autoS.automationEntityType = 'automationStatus'    AND \
         autoS.automationEntityID = auto.automationStatus) AS A \
        where ?? like ? ORDER BY automationCreationDate  DESC",[field,title]);
        return await appServices.runQuery(connection,query);
    },
    getEditAutomation: async function(connection,id){
        /*const query = mysql.format("select automationTower as Tower, automationTitle,\
        automationDescription from automationdetails where automationID=?",[id]);*/
        const query=mysql.format("SELECT ad.automationID,ad.automationTower,ad.automationApplication,\
        ad.automationTitle,ad.automationDescription,ad.automationStatus,ad.automationTickets,\
        aestatus.automationEntityValue as autoStatus,DATE_FORMAT(ad.automationDate,'%Y-%m-%d') as automationDate,ad.automationLead,\
        ad.automationFTE as automationFTE, ad.automationcostperday as automationcostperday,\
        ad.automationnetcostsaving as automationnetcostsaving , ad.automationticket  as automationticket, \
        ad.automationResource,DATE_FORMAT(ad.automationActualCompletionDate,'%Y-%m-%d') as CompletionDate,\
        ad.automationEfforts,ad.automationImplementation ,ad.automationWorkType,aewt.automationEntityValue, ad.automationBenefitArea,aeBenefit.automationEntityValue, ad.automationInitiativeCat,aeInicat.automationEntityValue,\
        DATE_FORMAT(ad.automationCreationDate,'%Y-%m-%d') as CreationDate,ad.automationCreatedBy,DATE_FORMAT(ad.automationModifiedDate,'%Y-%m-%d')as ModifiedDate,ad.automationModifiedBy,ad.automationComments,ad.automationTool,ad.automationInitiativeCat\
        FROM `automationdetails` ad join `automationentity` aestatus ON ad.automationStatus=aestatus.automationEntityID and aestatus.automationEntityType='automationStatus' \
        join `automationentity` aeWt on aewt.automationEntityID=ad.automationWorkType and aewt.automationEntityType='automationWorkType' \
        join `automationentity` aeBenefit on aeBenefit.automationEntityID=ad.automationBenefitArea and aeBenefit.automationEntityType='automationBenefitArea' \
        join `automationentity` aeInicat on aeInicat.automationEntityID=ad.automationInitiativeCat and aeInicat.automationEntityType='automationInitiativeCat' \
        where ad.automationID=?",[id]);
        return await appServices.runQuery(connection,query);
    },

       updateAutomationDetails : async function(connection,status,comment,lead,resource,automationWorkType,automationBenefitArea,automationInitiativeCat,automationTool,automationModifiedUser,
        creationdate,completiondate,automationEffort,automationFTE,automationcostperday,automationnetcostsaving,automationticket,automationImplementation,id) {
        const query = mysql.format("update automationDetails set automationStatus = ?,automationComments=?,automationlead=?,automationResource=?,automationworkType=?,automationBenefitArea=?,\
        automationInitiativeCat=?,automationTool=?,automationModifiedBy=?,automationModifiedDate=sysdate(),\
        automationActualCompletionDate=?,automationEfforts=?,automationFTE=?,automationcostperday=?,\
        automationnetcostsaving=?,automationticket=?,automationImplementation=? where automationID = ?",
        [status,comment,lead,resource,automationWorkType,automationBenefitArea,automationInitiativeCat,
            automationTool,automationModifiedUser,completiondate,automationEffort,automationFTE,
            automationcostperday,automationnetcostsaving,automationticket,automationImplementation,id]);
        return await appServices.runQuery(connection,query);
    },
    insertAutomationHistory: async function(connection,data){
        const query = mysql.format("insert into automationhistorycomments set ? ",[data]);
        return await appServices.runQuery(connection,query);
    },
    getAutomationHistory: async function(connection,id){
        const query = mysql.format("SELECT autodetailsID,autodetailsComments,autodetailsUser,\
        DATE_FORMAT(autodetailsCreatedDate,'%Y-%m-%d') AS autodetailsCreatedDate \
        FROM automationhistorycomments \
         WHERE `autodetailsID`= ? order by autohistoryID",[id]);
        return await appServices.runQuery(connection,query);
    }
}
