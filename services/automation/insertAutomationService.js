const appServices = require('../appService');
const mysql = require('mysql');

module.exports = {
    // inserting ideas into table 
    insertAutomation: async function(connection,data){
        const query = mysql.format("insert into automationdetails set ? ",[data]);
        return await appServices.runQuery(connection,query);
    }
}