const appServices = require('../appService');
const mysql = require('mysql');

module.exports = {
    getAutomationDetails: async function(connection){
        const query = "SELECT automationID,automationTower,automationApplication,automationTitle,\
        DATE_FORMAT(automationActualCompletionDate,'%Y-%m-%d')  as automationDate,\
        automationDescription ,automationEfforts  ,automationImplementation ,automationFTE ,automationcostperday ,\
        automationnetcostsaving ,automationticket ,\
        autow.automationEntityValue AS WorkType,\
        autoB.automationEntityValue AS BenefitArea, autoT.automationEntityValue AS Tool,\
        autoI.automationEntityValue AS Initiative,\
        autoS.automationEntityValue AS STATUS, \
        automationCreationDate  FROM \
        automationdetails auto  JOIN automationentity autoW ON autow.automationEntityType = 'automationWorkType' AND autow.automationEntityID = auto.automationWorkType \
    JOIN automationentity autoB ON autoB.automationEntityType = 'automationBenefitArea' AND autoB.automationEntityID = auto.automationBenefitArea \
    JOIN automationentity autoI ON autoI.automationEntityType = 'automationInitiativeCat' AND autoI.automationEntityID = auto.automationInitiativeCat \
    JOIN automationentity autoS ON autoS.automationEntityType = 'automationStatus' AND autoS.automationEntityID = auto.automationStatus \
    JOIN automationentity autoT ON autoT.automationEntityType = 'automationTool' AND autoT.automationEntityID = auto.automationTool";
        return await appServices.runQuery(connection,query);
    },
    getAutomationTower: async function(connection){
        const query = "SELECT automationtowerID as ID,automationTowerName as Value FROM `automationtowers` WHERE automationTowerStatus = 1";
        return await appServices.runQuery(connection,query);
    },
    getAutomationAppName: async function(connection){
        const query = "SELECT automationapplicationID AS ID,automationapplicationname AS Value, \
        ATS.automationTowerName AS TowID FROM automationapplicationname AAN\
        join automationtowers ATS on ATS.automationTowerID = AAN.automationAppTowerID WHERE automationAppStatus=1 ";
        return await appServices.runQuery(connection,query);
    },
    getAutomationPeriod: async function(connection,datePeriod){
        const query = "select * from ideas ORDER BY created_at DESC";
        return await appServices.runQuery(connection,query);
    },
    getAutomationEntity: async function(connection){
        const query = "SELECT automationEntityID AS ID,automationEntityType AS Type,\
        automationEntityValue AS Value FROM `automationentity` WHERE automationEntityStatus = 1";
        return await appServices.runQuery(connection,query);
    },
    getAutomationStatus: async function(connection){
        const query = mysql.format("SELECT automationEntityID as ID,automationEntityValue as Value FROM `automationentity` WHERE automationEntityType='automationStatus'");
        return await appServices.runQuery(connection,query);
    }
}   