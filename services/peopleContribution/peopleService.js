const appServices = require('../appService');
const mysql = require('mysql');
module.exports = {

    getAllTraining: async function(connection){
        const query = "select TrainingId,TrainingName,TrainingDescription,TrainingMode,TrainingType,\
        DATE_FORMAT(TrainingStartDate,'%Y-%m-%d') as TrainingStartDate,DATE_FORMAT(TrainingEndDate,'%Y-%m-%d') as TrainingEndDate,TargetAudience,Location,LearningCategory,Prerequisite,TrainingComments,Contributor,CreatedDate,modifiedDate,\
        TrainingStatus,TrainingSkillType,TrainingDuration from trainingdetails ORDER BY TrainingStartDate DESC";
        return await appServices.runQuery(connection,query);
    },
    getUpcomingTraining: async function(connection){
        const query = "SELECT * FROM `trainingdetails` WHERE TRainingStartDate>sysdate() and TrainingStatus='Scheduled'";
        return await appServices.runQuery(connection,query);
    },
    getTrainingBetweenDates: async function(connection,sDate1,sDate2){
        const query = mysql.format("SELECT * from trainingdetails\
        where TrainingStartDate>=DATE_FORMAT(?,'%Y-%m-%d')\
        and TrainingStartDate<=DATE_FORMAT(?,'%Y-%m-%d') ORDER BY auto.automationCreationDate  DESC",[sDate1,sDate2]);
        return await appServices.runQuery(connection,query);
    },
    getTraingbyMode:async function(connection,mode){
        const query=mysql.format("select * from trainingdetails where TrainingMode=?",[mode]);
        return await appServices.runQuery(connection,query);
    },
    getTraingbySkill:async function(connection,TrainingSkillType){
        const query=mysql.format("select * from trainingdetails where TrainingSkillType=?",[TrainingSkillType]);
        return await appServices.runQuery(connection,query);
    },
    getTrainingdetail:async function(connection,id){
        const query=mysql.format("select TrainingId,TrainingName,TrainingDescription,TrainingMode,TrainingType,\
        DATE_FORMAT(TrainingStartDate,'%Y-%m-%d') as TrainingStartDate,DATE_FORMAT(TrainingEndDate,'%Y-%m-%d') as TrainingEndDate,TargetAudience,Location,LearningCategory,Prerequisite,TrainingComments,Contributor,CreatedDate,modifiedDate,\
        TrainingStatus,TrainingSkillType,TrainingDuration from trainingdetails where TrainingId=?",[id]);
        return await appServices.runQuery(connection,query);
    },
    updateTrainingDetail:async function(connection,TrainingDescription,TrainingMode,TrainingType,TrainingSkillType,TrainingStartDate,TrainingEndDate,TargetAudience,TrainingLocation,TrainingPrequisite,TrainingComments,id){ 
     const query=mysql.format("update trainingdetails set TrainingDescription=?,TrainingMode=?,TrainingType=?,TrainingSkillType=?,TrainingStartDate=?,TrainingEndDate=?,TargetAudience=?,Location=?,Prerequisite=?,TrainingComments=?,modifiedDate=sysdate()\
      where TrainingId=?",[TrainingDescription,TrainingMode,TrainingType,TrainingSkillType,TrainingStartDate,TrainingEndDate,TargetAudience,TrainingLocation,TrainingPrequisite,TrainingComments,id]);
     return await appServices.runQuery(connection,query);   
    },
    getTrainingByEmpID:async function(connection,id){
        const query=mysql.format("SELECT trngDetails.TrainingId,trngDetails.TrainingStatus,trngDetails.TargetAudience,trngEnroll.EmployeeID, trngEnroll.EnrollmentStatus FROM `trainingenrollment` trngEnroll inner JOIN trainingdetails trngDetails on trngEnroll.TrainingId=trngDetails.TrainingId WHERE employeeid=?",[id]);
        return await appServices.runQuery(connection,query);
    },
    getEnrolledUserCount:async function (connection) {
        const query=mysql.format("select TrainingId,count(*) as usrCount from trainingenrollment GROUP by TrainingId");
        return await appServices.runQuery(connection,query);
    },
    getEnrolledUserCountBySkillType:async function(connection){
    const query=mysql.format("SELECT trndtls.TrainingSkillType,COUNT(trngenroll.EmployeeID)as EmployeeCount FROM `trainingenrollment` trngenroll inner join trainingdetails trndtls on trngenroll.TrainingId=trndtls.TrainingId group by trndtls.TrainingSkillType ");
    return await appServices.runQuery(connection,query);
    },
    getEnrolledUserCountbyMonth:async function(connection){
    const query=mysql.format("SELECT trndtls.TrainingSkillType,Date_Format(trndtls.TrainingStartDate,'%Y-%m' )as trngStrtDate,count(trngenroll.EmployeeID)as empcount FROM `trainingenrollment` trngenroll inner join trainingdetails trndtls on trngenroll.TrainingId=trndtls.TrainingId group by trngStrtDate,trndtls.TrainingSkillType");
        return await appServices.runQuery(connection,query);
    }

}