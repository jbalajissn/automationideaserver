const appServices = require('../appService');
const mysql = require('mysql');

module.exports = {
    // inserting ideas into table 
    insertpeopleContribution: async function(connection,data){
        const query = mysql.format("insert into TrainingDetails set ? ",[data]);
        return await appServices.runQuery(connection,query);
    },
    TrainingEnrollment:async function(connection,data){
        const query=mysql.format("insert into trainingenrollment set ?",[data]);
        return await appServices.runQuery(connection,query);
    }
    
}