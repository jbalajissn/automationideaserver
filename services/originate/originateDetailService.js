const appServices = require('../appService');
const mysql = require('mysql');

module.exports = {
    getOriginateDetails: async function(connection){
        const query = "SELECT `OriginateID`,`OriginateTower`, `OriginateCategoryType`,`OriginateStatusType`,`OriginateTitle`,\
        `OriginateDesc`,`OriginateACNStakeholder`,`OriginateCLNStakeholder`,`OriginateTechnology`,\
        `OriginateETCDate`,`OriginateProbability`,`OriginateEstimate`,`OriginateRevenue`,\
        `ProposedCCI`,`ApprovedCCI`,`OriginateApprover` FROM `originatedetails`\
        order by OriginateCreatedDate desc";
        return await appServices.runQuery(connection,query);;
    },
    getOriginateEntity: async function(connection){
        const query = "SELECT originateentityID,originateentityTitle,originateentityDesc FROM originateentity \
         WHERE originateentitystatus=true";
        return await appServices.runQuery(connection,query);
    },
    getSearchOriginate: async function(connection,title){
        const query = mysql.format("SELECT `OriginateID`,`OriginateTower`, `OriginateCategoryType`,`OriginateStatusType`,`OriginateTitle`,\
        `OriginateDesc`,`OriginateACNStakeholder`,`OriginateETCDate` FROM `originatedetails` \
        where OriginateTitle like ? ORDER BY OriginateCreatedDate  DESC",[title]);
        return await appServices.runQuery(connection,query);
    },
    getCommentsOriginate: async function(connection,id){
        const query=mysql.format("SELECT Originatecomments,OriginateCommentsBy,DATE(OriginateCommentsDate) AS OriginateCommentsDate FROM originatecomments \
         WHERE OriginateID= ? ORDER BY OriginateCommentsDate  DESC",[id]);
        return await appServices.runQuery(connection,query);
    }   
}   