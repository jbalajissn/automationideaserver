const appServices = require('../appService');
const mysql = require('mysql');

module.exports = {
    insertOriginate: async function(connection,data){
        const query = mysql.format("insert into originatedetails  set ? ",[data]);
        var result = await appServices.runQuery(connection,query);
        return result.insertId;
    },
    updateOriginate : async function(connection,OriginateDesc,OriginateCategoryType,
        OriginateACNStakeholder,OriginateCLNStakeholder,OriginateETCDate,OriginateStatusType,ProposedCCI,ApprovedCCI,
        OriginateProbability,OriginateEstimate,OriginateRevenue,OriginateApprover,OriginateComments,OriginateID,)  
        {
        const query = mysql.format("UPDATE `originatedetails` SET `OriginateDesc`=?,`OriginateCategoryType`=?,\
        `OriginateACNStakeholder`=?,`OriginateCLNStakeholder`=?,`OriginateETCDate`=?,`OriginateStatusType`=?,\
        `ProposedCCI`=?,`ApprovedCCI`=?,`OriginateProbability`=?,`OriginateEstimate`=?,`OriginateRevenue`=?,\
        `OriginateApprover`=?,`OriginateComments`=? where originateID = ?",
        [OriginateDesc,OriginateCategoryType,OriginateACNStakeholder,
            OriginateCLNStakeholder, OriginateETCDate,OriginateStatusType,ProposedCCI,ApprovedCCI,
            OriginateProbability,OriginateEstimate,OriginateRevenue,OriginateApprover,OriginateComments,OriginateID]);
        return await appServices.runQuery(connection,query);
    },
    insertOriginateComments: async function(connection,data){
        const query = mysql.format("insert into originatecomments  set ? ",[data]);
        return await appServices.runQuery(connection,query);
    }
}